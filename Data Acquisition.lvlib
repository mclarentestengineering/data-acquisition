﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="Parent" Type="Folder">
		<Item Name="DAQ.lvclass" Type="LVClass" URL="../DAQ/DAQ.lvclass"/>
	</Item>
	<Item Name="Daqmx" Type="Folder">
		<Item Name="Channels" Type="Folder">
			<Item Name="Analog" Type="Folder">
				<Item Name="Thermocouple.lvclass" Type="LVClass" URL="../Channels/Thermocouples/Thermocouple.lvclass"/>
				<Item Name="Voltage.lvclass" Type="LVClass" URL="../Channels/Voltage/Voltage.lvclass"/>
				<Item Name="Current.lvclass" Type="LVClass" URL="../Channels/Current/Current.lvclass"/>
				<Item Name="Accelerometer.lvclass" Type="LVClass" URL="../Channels/Accelerometer/Accelerometer.lvclass"/>
				<Item Name="RTDs.lvclass" Type="LVClass" URL="../Channels/RTDs/RTDs.lvclass"/>
				<Item Name="Microphone.lvclass" Type="LVClass" URL="../Channels/Microphone/Microphone.lvclass"/>
			</Item>
			<Item Name="Counter" Type="Folder">
				<Item Name="Angular Speed Encoder.lvclass" Type="LVClass" URL="../Channels/Angular Speed Encoder/Angular Speed Encoder.lvclass"/>
				<Item Name="Frequency.lvclass" Type="LVClass" URL="../Channels/Frequency/Frequency.lvclass"/>
			</Item>
			<Item Name="Digital" Type="Folder">
				<Item Name="Digital Line Input.lvclass" Type="LVClass" URL="../Channels/Digital Line Input/Digital Line Input.lvclass"/>
			</Item>
			<Item Name="Channels.lvclass" Type="LVClass" URL="../Channels/Parent/Channels.lvclass"/>
		</Item>
		<Item Name="DAQmx.lvclass" Type="LVClass" URL="../DAQmx/DAQmx.lvclass"/>
		<Item Name="Analog Input.lvclass" Type="LVClass" URL="../Analog Input/Analog Input.lvclass"/>
		<Item Name="Counter Input.lvclass" Type="LVClass" URL="../Counter Input/Counter Input.lvclass"/>
		<Item Name="Digital Input.lvclass" Type="LVClass" URL="../Digital Input/Digital Input.lvclass"/>
	</Item>
	<Item Name="Test" Type="Folder">
		<Item Name="Type Defs" Type="Folder">
			<Item Name="Channel Type.ctl" Type="VI" URL="../Tests/Type Defs/Channel Type.ctl"/>
		</Item>
		<Item Name="Test Daqmx.vi" Type="VI" URL="../Tests/Test Daqmx.vi"/>
	</Item>
	<Item Name="XNET" Type="Folder">
		<Item Name="XNET.lvclass" Type="LVClass" URL="../XNET/XNET.lvclass"/>
	</Item>
	<Item Name="Utilities" Type="Folder">
		<Item Name="Timer" Type="Folder">
			<Item Name="Timer.lvclass" Type="LVClass" URL="../Timer/Timer.lvclass"/>
		</Item>
	</Item>
</Library>
