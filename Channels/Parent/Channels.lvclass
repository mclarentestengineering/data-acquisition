﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">Data Acquisition.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../Data Acquisition.lvlib</Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91M&lt;/W-,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@P_W`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"&lt;Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"O[^NF!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"4B5F.31QU+!!.-6E.$4%*76Q!!%,A!!!1J!!!!)!!!%*A!!!!M!!!!!B:%982B)%&amp;D=86J=WFU;7^O,GRW&lt;'FC%%.I97ZO:7RT,GRW9WRB=X-!!!#A&amp;Q#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!!`W*[))\*6)J`1^KL,&amp;Y'!!!!!-!!!!%!!!!!"2`5ONVJ&lt;*4\I`PI/$&lt;@1!V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!]D&lt;&gt;Z&lt;=73EO&lt;"&gt;QU2:Y=$1%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!""Q[;*&lt;_A*N]O:3+28QGYU:!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!)1!!!"BYH'.A9W"K9,D!!-3-$EQ.4"F!VA?'!!9!0[%&amp;FA!!!!!!!%5!!!%9?*RD9-!%`Y%!3$%S-,!)!'E7.(%Q$7.4%_!S&amp;Z&gt;&gt;5(&amp;GK"N:9=*!I2&gt;!GAEE"V8$!:&amp;CY5$S$BSQ9T%&lt;!*??*B!!!!!!!!!-!!&amp;73524!!!!!!!$!!!":1!!!T2YH%NA:'!ILT!T_1#EG9&amp;9H+'")4E`*:7,!=BH1)!`4!Q5A1;I?6JIYI9($K="A2[`@!O9\^(]2M/T_1&gt;4K;$($JC;`R=]GI^I(0&lt;I&lt;A1*(8&gt;)!#PM:D3]Z'JYY0_%#W"N1!8]]AZA`1W6VAQF\)9(Q),('\]Q1IT"-"#G(C(BUXH1J`-5S&amp;3)CL1*90H/#RY((\"YQJ6Z*9MQ_$!?]/BUY4)Y\M)6)!9SH@%5&amp;AM9QI!'+(BUHE0)\7+%+/BG]^A*%`0I^!B!Y7EA]TL!0E17_)!O]!$&amp;@U$,&amp;?)/9YW(N;`P\7)"UGR)9AY-E0BZQ)C+^2A9'5#/":'Z5,5W1$946%Q'+A:CHY/S.:$UP),,W]0&amp;WJ$MB)GN"?O$O)%.+P90SA?R?9!+!["M+3"\!:3N$71,1.EW)$&gt;$W&lt;Z1^A.IR//CH@V&gt;8'&amp;M:!",[\!ETQH%S&lt;E&amp;"A:[V='V1$9!G27#]A!!!!!!!)]!!!$Y?*RT9'"A++]Q-ZE!J*E:'2D%'2I9EP.45BH1Q"&amp;U!3DQ;(ZDY&gt;(JIW,AU2GCIO(2,?42T?GR!S&lt;\`]*/I*E-T4]9_;==Y.^WQ)0@Z;"(]R',QTB-9W"9_`L?,J!72C1R"S"79J"AA)G,)9G$A,/`CWM$&amp;L.A&lt;A&lt;JY14CZ.Q#!Q/^[O";)"M!SMAB$Q!!!!!-&amp;Q#!%1!!"$%X,D!!!!!!$"=!A!!!!!1R.SYQ!!!!!!Q8!)!2!!!%-4=O-!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$"=!A"%!!!1R.SYQ!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9"A!!'"G!!"BA9!!:A"A!'A!%!"M!$!!;Q$1!'D$M!"I06!!;!KQ!'A.5!"I#L!!;!V1!'A+M!"I$6!!:ALA!''.A!"A&lt;A!!9"A!!(`````!!!%!0```````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!$U^!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!$WSB\+S01!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!$WSBW*C9G+SMDU!!!!!!!!!!!!!!!!!!!!!``]!!$WSBW*C9G*C9G*CML)^!!!!!!!!!!!!!!!!!!$``Q#SBW*C9G*C9G*C9G*C9L+S!!!!!!!!!!!!!!!!!0``!)?(9G*C9G*C9G*C9G*C`L)!!!!!!!!!!!!!!!!!``]!B\+SBW*C9G*C9G*C`P\_BQ!!!!!!!!!!!!!!!!$``Q#(ML+SMI&gt;C9G*C`P\_`P[(!!!!!!!!!!!!!!!!!0``!)?SML+SML+(MP\_`P\_`I=!!!!!!!!!!!!!!!!!``]!B\+SML+SML,_`P\_`P\_BQ!!!!!!!!!!!!!!!!$``Q#(ML+SML+SMP\_`P\_`P[(!!!!!!!!!!!!!!!!!0``!)?SML+SML+S`P\_`P\_`I=!!!!!!!!!!!!!!!!!``]!B\+SML+SML,_`P\_`P\_BQ!!!!!!!!!!!!!!!!$``Q#(ML+SML+SMP\_`P\_`P[(!!!!!!!!!!!!!!!!!0``!,+SML+SML+S`P\_`P\_ML)!!!!!!!!!!!!!!!!!``]!!)?(ML+SML,_`P\_ML+(!!!!!!!!!!!!!!!!!!$``Q!!!!#(ML+SMP\_ML+(!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!B\+SML*C!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!)&gt;C!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!!-!!&amp;'5%B1!!!!!!!$!!!$/1!!"PBYH+W6TUN5524(TRV(O4-KXD@_H."'Z=UEF2B";F)WZ&lt;85"F&amp;5L#"L;#18K6191K!N(I),.U5O!K'&amp;#"(21K26%$&amp;;]";Z+N*AUH_AAM1CX\T/@7`?`,+G&amp;M\C=:GZX`/^^XM_:RZ!TH&gt;7:IP#B!;%&lt;?%CI)%TJ"+!=#W&amp;W-=X";S0`!*3Z#9;_'E@7\&gt;&amp;3&lt;E'?3(62Y`Q;@C+O`80_HVY3!D&lt;Q+UZT)X&amp;H"I5B.23K5//-0FZO4S&gt;&lt;676I),.E+DNAOT:JD0++"K#5C/?5CW*!O&amp;6&gt;LN3X25='62E];WDFLK.EAY.'&amp;@T&lt;]G2!VA2L6]&lt;*=E!T*.2KS2AS2J98&amp;R-C#24Z$//U3AU]VBWC1REU,CY7H&amp;&lt;DBQW.%Z$ATZ,FA`X2"_)MQN2OL31KQSFK"OXLDRP?8'0ZD&gt;UGZO&lt;K-.H4$?E1:%=/50&gt;&gt;#/UFJX@L&lt;Q!!C2]B?I@^#&gt;#,X7+,BB\8&gt;A)&lt;R.B0&lt;DOU?#AINKOANXS;I._IQVWKQUHM!XEL.%'"Z^GE/B$@_9_6.K!6X&amp;\O$K@"W]/DV?W$!6(2A:P\/\+3;Y['M3.B9%"""3$(4BU*3=]#AM,#XBJ@#;ET3ANF3.RH74=I$??=CC2MH".J(5+U_)`?K_,R,R.NF2/4S=Y09K=3CT^7#G=(NN\4OM2HYEU4C%!&gt;8!Z!X-.JCC*UTJ5_3'11&gt;/)GME54OP1RW`Z:/,U_'Z/[SSP/+?TM\-J/ES_+=[JH2#45W66X^&amp;X"+U`^7`1$.?3;/X'&gt;IC`C#^9@T`74YH_X&amp;`9,!4@@\'J6(='BW0Z-[`JC&lt;_$8%0QC.ZY3X)V;//K'Y^?9$J2Y&lt;10;@#"%[`Z=?U48H.M[L(E-5@($'(\L&gt;$(VG_3VCN*[W8K&gt;HA30OXIUSR(\JA_LY20#@I=-HT195^]-,I/H*R[/:+&lt;.(2FBE_&lt;V5*&gt;_F.U3B8=Q_$_'2E3=DZN/)O3%:%^7Q9B4Z]*MH2&gt;4R!31%+]&amp;]&amp;\#8"#F\VX&lt;?;1:PGGMP!/H5EYU`76M@C\BP&lt;)8-_UA&lt;6C`5E.CHG,[RVLR\0HU8&lt;;SO:QV((K3_A=X1K`D\_.QI'Y5FEF,PJ3@O3B:&lt;]"#5,Q:A!!!!!!!!1!!!!J!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!F)!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!%Q8!)!!!!!!!1!)!$$`````!!%!!!!!!$!!!!!#!!Z!-0````]%4G&amp;N:1!!'E"1!!%!!""$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!!"!!%!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!+2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!1!!!!!!!!!!!!!!'UR71WRB=X.1=GFW982F2'&amp;U962J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.&lt;J=]%!!!!!!!!!*ER71WRB=X.1=GFW982F2'&amp;U95RB=X2"=("M;76E6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!VOFTQ1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6(FQ:52F=W-!!!"-&amp;Q#!!!!!!!%!#!!Q`````Q!"!!!!!!!Q!!!!!A!/1$$`````"%ZB&lt;75!!"J!5!!"!!!11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!!!1!"!!!!!!!!!"Z-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U96.J?G5!!!!:&amp;Q#!!!!!!!%!"1!$!!!"!!!!!!!)!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;%:GRU2'&amp;U91!!!$Q8!)!!!!!!!A!/1$$`````"%ZB&lt;75!!"J!5!!"!!!11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!!!1!"!!!!!!!!!!!!!!!!!!1!!Q!)!!!!"!!!!%Q!!!!I!!!!!A!!"!!!!!!#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/]!!!&amp;G?*RVDUV+QV!5B&lt;`EJ7V;ERL`CDD+#E2Q!Z%7BV+=/07:(QW]*J3]3I&gt;OT5WY"Y?O1'`3K!0R(&lt;BQTO'&gt;=S^QQD3Z_*4HX?B6$H&gt;]P,UXLY#;,&lt;46]67[XJ2.;=O[/D@0JHS)ZE_[KH,4#%W.&lt;JLA2UCNY3R:YE#YU/P6.OYN"A)G:"*\CCN%^:2,0-&lt;*#&amp;792`TE2&gt;7X&gt;C@E&gt;5%E;9IB)XSVS1J]OG5*OJ;*MNN-=J93/7;0W4`&gt;,D+'\2$8ZVZK0;\F7]S5@&lt;&amp;&lt;2+*^Q_XR6`FV$DLMDH!Y&amp;+5^S:&amp;V"Q3%((%M-Y1P&lt;ZM[OQ!!!!"_!!%!!A!$!!5!!!"9!!]%!!!!!!]!W!$6!!!!91!0"!!!!!!0!.A!V1!!!'I!$Q1!!!!!$Q$9!.5!!!"TA!#%!)!!!!]!W!$6!!!!&gt;1!3"!!!!!!2!/]!Z!B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q#&amp;.F:W^F)&amp;6*!!"35V*$$1I!!UR71U.-1F:8!!!1O!!!"#E!!!!A!!!1G!!!!!!!!!!!!!!!)!!!!$1!!!1=!!!!'UR*1EY!!!!!!!!"6%R75V)!!!!!!!!";&amp;*55U=!!!!!!!!"@%.$5V1!!!!!!!!"E%R*&gt;GE!!!!!!!!"J%.04F!!!!!!!!!"O&amp;2./$!!!!!!!!!"T%2'2&amp;-!!!!!!!!"Y%R*:(-!!!!!!!!"^&amp;:*1U1!!!!"!!!##(:F=H-!!!!%!!!#-&amp;.$5V)!!!!!!!!#F%&gt;$5&amp;)!!!!!!!!#K%F$4UY!!!!!!!!#P'FD&lt;$A!!!!!!!!#U%R*:H!!!!!!!!!#Z%:13')!!!!!!!!#_%:15U5!!!!!!!!$$&amp;:12&amp;!!!!!!!!!$)%R*9G1!!!!!!!!$.%*%3')!!!!!!!!$3%*%5U5!!!!!!!!$8&amp;:*6&amp;-!!!!!!!!$=%253&amp;!!!!!!!!!$B%V6351!!!!!!!!$G%B*5V1!!!!!!!!$L&amp;:$6&amp;!!!!!!!!!$Q%:515)!!!!!!!!$V!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!-!!!!!!!!!!!`````Q!!!!!!!!$5!!!!!!!!!!$`````!!!!!!!!!/A!!!!!!!!!!0````]!!!!!!!!!]!!!!!!!!!!!`````Q!!!!!!!!%=!!!!!!!!!!$`````!!!!!!!!!31!!!!!!!!!!0````]!!!!!!!!"4!!!!!!!!!!!`````Q!!!!!!!!'9!!!!!!!!!!$`````!!!!!!!!!;A!!!!!!!!!!@````]!!!!!!!!$&amp;!!!!!!!!!!%`````Q!!!!!!!!/I!!!!!!!!!!@`````!!!!!!!!!\A!!!!!!!!!#0````]!!!!!!!!$S!!!!!!!!!!*`````Q!!!!!!!!09!!!!!!!!!!L`````!!!!!!!!!_A!!!!!!!!!!0````]!!!!!!!!$_!!!!!!!!!!!`````Q!!!!!!!!11!!!!!!!!!!$`````!!!!!!!!"#1!!!!!!!!!!0````]!!!!!!!!%K!!!!!!!!!!!`````Q!!!!!!!!CM!!!!!!!!!!$`````!!!!!!!!#,Q!!!!!!!!!!0````]!!!!!!!!,`!!!!!!!!!!!`````Q!!!!!!!!Q%!!!!!!!!!!$`````!!!!!!!!$!Q!!!!!!!!!!0````]!!!!!!!!-(!!!!!!!!!!!`````Q!!!!!!!!S%!!!!!!!!!!$`````!!!!!!!!$)Q!!!!!!!!!!0````]!!!!!!!!/Z!!!!!!!!!!!`````Q!!!!!!!!\M!!!!!!!!!!$`````!!!!!!!!$P1!!!!!!!!!!0````]!!!!!!!!0)!!!!!!!!!#!`````Q!!!!!!!"!5!!!!!!R$;'&amp;O&lt;G6M=SZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B:%982B)%&amp;D=86J=WFU;7^O,GRW&lt;'FC%%.I97ZO:7RT,GRW9WRB=X.16%AQ!!!!!!!!!!!!!!!#!!%!!!!!!!!!!!!!!1!91&amp;!!!""$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!!"!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!!!!%!!!!!!!%!!!!!!A!/1$$`````"%ZB&lt;75!!&amp;=!]&gt;&lt;J=]%!!!!$&amp;E2B&gt;'%A17.R&gt;7FT;82J&lt;WYO&lt;(:M;7)11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=QR$;'&amp;O&lt;G6M=SZD&gt;'Q!'E"1!!%!!!V%98&amp;N?#"$;'&amp;O&lt;G6M!!%!!1!!!!(`````!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;Q#!!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="Channels.ctl" Type="Class Private Data" URL="Channels.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Dynamic Accessors" Type="Folder">
		<Item Name="Name" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">Name</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">Name</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read Name.vi" Type="VI" URL="../Methods/Dynamic Accessors/Read Name.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!-0````]%4G&amp;N:1!!1%"Q!"Y!!#E72'&amp;U93""9X&amp;V;8.J&gt;'FP&lt;CZM&gt;GRJ9B"$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!R$;'&amp;O&lt;G6M=S"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!0E"Q!"Y!!#E72'&amp;U93""9X&amp;V;8.J&gt;'FP&lt;CZM&gt;GRJ9B"$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!N$;'&amp;O&lt;G6M=S"J&lt;A"5!0!!$!!$!!1!"1!'!!1!"!!%!!1!"Q!%!!1!#!)!!(A!!!U)!!!!!!!!#1!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!!!!!!E!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
			<Item Name="Write Name.vi" Type="VI" URL="../Methods/Dynamic Accessors/Write Name.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J&amp;E2B&gt;'%A17.R&gt;7FT;82J&lt;WYO&lt;(:M;7)11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!-1WBB&lt;GZF&lt;(-A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!Z!-0````]%4G&amp;N:1!!0E"Q!"Y!!#E72'&amp;U93""9X&amp;V;8.J&gt;'FP&lt;CZM&gt;GRJ9B"$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!N$;'&amp;O&lt;G6M=S"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!)1!!!!EA!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Methods" Type="Folder">
		<Item Name="VIs for Override" Type="Folder">
			<Item Name="Add Channel to Task.vi" Type="VI" URL="../Methods/VIs for Override/Add Channel to Task.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'4!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J&amp;E2B&gt;'%A17.R&gt;7FT;82J&lt;WYO&lt;(:M;7)11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!-1WBB&lt;GZF&lt;(-A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"Y!.`````]!#2=!A!!!!!!"!!1!!!!"!!!!!!!!!$J!=!!6"&amp;2B=WM!!!%!"Q6/352"52=!A!!!!!!"!!1!!!!"!!!!!!!!$U2"57VY)&amp;2B=WMA4G&amp;N:1!_1(!!(A!!+2:%982B)%&amp;D=86J=WFU;7^O,GRW&lt;'FC%%.I97ZO:7RT,GRW9WRB=X-!#U.I97ZO:7RT)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!#!!*!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!A!!!#1!!!!!!%!#A!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
			</Item>
			<Item Name="Channel Settings.vi" Type="VI" URL="../Methods/VIs for Override/Channel Settings.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%\!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J&amp;E2B&gt;'%A17.R&gt;7FT;82J&lt;WYO&lt;(:M;7)11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!-1WBB&lt;GZF&lt;(-A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J&amp;E2B&gt;'%A17.R&gt;7FT;82J&lt;WYO&lt;(:M;7)11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!,1WBB&lt;GZF&lt;(-A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
			</Item>
			<Item Name="Destroy Channel.vi" Type="VI" URL="../Methods/VIs for Override/Destroy Channel.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%\!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J&amp;E2B&gt;'%A17.R&gt;7FT;82J&lt;WYO&lt;(:M;7)11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!-1WBB&lt;GZF&lt;(-A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J&amp;E2B&gt;'%A17.R&gt;7FT;82J&lt;WYO&lt;(:M;7)11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!,1WBB&lt;GZF&lt;(-A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
			</Item>
			<Item Name="Get Physical Channels.vi" Type="VI" URL="../Methods/VIs for Override/Get Physical Channels.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'0!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$:!.`````]!$B=!A!!!!!!"!!1!!!!"!!!!!!!!&amp;E2"57VY)&amp;"I?8.J9W&amp;M)%.I97ZO:7Q!!"Z!1!!"`````Q!&amp;%6"I?8.J9W&amp;M)%.I97ZO:7RT!%"!=!!?!!!J&amp;E2B&gt;'%A17.R&gt;7FT;82J&lt;WYO&lt;(:M;7)11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!-1WBB&lt;GZF&lt;(-A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J&amp;E2B&gt;'%A17.R&gt;7FT;82J&lt;WYO&lt;(:M;7)11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!,1WBB&lt;GZF&lt;(-A;7Y!6!$Q!!Q!!Q!%!!9!"Q!%!!1!"!!%!!A!"!!%!!E#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!+!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
			</Item>
			<Item Name="Read Data.vi" Type="VI" URL="../Methods/VIs for Override/Read Data.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;S!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!6!!$#&amp;&gt;B&gt;G6G&lt;X*N!!!71%!!!@````]!"1AR2#""=H*B?1!!1%"Q!"Y!!#E72'&amp;U93""9X&amp;V;8.J&gt;'FP&lt;CZM&gt;GRJ9B"$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!R$;'&amp;O&lt;G6M=S"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!%5!+!!N497VQ&lt;'5A5G&amp;U:1!_1(!!(A!!+2:%982B)%&amp;D=86J=WFU;7^O,GRW&lt;'FC%%.I97ZO:7RT,GRW9WRB=X-!#U.I97ZO:7RT)'FO!&amp;1!]!!-!!-!"!!'!!=!"!!%!!1!"!!)!!1!#1!+!A!!?!!!$1A!!!!!!!!*!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!A!!!#1!!!!!!%!#Q!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
			</Item>
			<Item Name="Set Clock.vi" Type="VI" URL="../Methods/VIs for Override/Set Clock.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!''!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!!Z!-0````]&amp;6'&amp;T;X-!1%"Q!"Y!!#E72'&amp;U93""9X&amp;V;8.J&gt;'FP&lt;CZM&gt;GRJ9B"$;'&amp;O&lt;G6M=SZM&gt;G.M98.T!!R$;'&amp;O&lt;G6M=S"P&gt;81!!#"!5!!$!!!!!1!#%W6S=G^S)'FO)#BO&lt;S"F=H*P=CE!*E!Q`````RR%16&amp;N9S"1;(FT;7.B&lt;#"$&lt;'^D;S"$;'&amp;O&lt;G6M!!!81!I!%&amp;.B&lt;8"M:3"3982F)#B)?CE!!$Z!=!!?!!!J&amp;E2B&gt;'%A17.R&gt;7FT;82J&lt;WYO&lt;(:M;7)11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!,1WBB&lt;GZF&lt;(-A;7Y!6!$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!#!!*!!I#!!"Y!!!.#!!!!!!!!!E!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!))!!!!#!!!!*!!!!!!!1!,!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
			</Item>
			<Item Name="Set Physical Channel.vi" Type="VI" URL="../Methods/VIs for Override/Set Physical Channel.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;R!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J&amp;E2B&gt;'%A17.R&gt;7FT;82J&lt;WYO&lt;(:M;7)11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!-1WBB&lt;GZF&lt;(-A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$:!.`````]!$B=!A!!!!!!"!!1!!!!"!!!!!!!!&amp;E2"57VY)&amp;"I?8.J9W&amp;M)%.I97ZO:7Q!!$Z!=!!?!!!J&amp;E2B&gt;'%A17.R&gt;7FT;82J&lt;WYO&lt;(:M;7)11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!,1WBB&lt;GZF&lt;(-A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!##!!!!*!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">1090519168</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
			</Item>
			<Item Name="Stop Channel.vi" Type="VI" URL="../Methods/VIs for Override/Stop Channel.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%\!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!%"!=!!?!!!J&amp;E2B&gt;'%A17.R&gt;7FT;82J&lt;WYO&lt;(:M;7)11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!-1WBB&lt;GZF&lt;(-A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$Z!=!!?!!!J&amp;E2B&gt;'%A17.R&gt;7FT;82J&lt;WYO&lt;(:M;7)11WBB&lt;GZF&lt;(-O&lt;(:D&lt;'&amp;T=Q!,1WBB&lt;GZF&lt;(-A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1342714368</Property>
			</Item>
		</Item>
	</Item>
</LVClass>
